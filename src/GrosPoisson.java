import java.util.ArrayList;
import java.util.Random;

class GrosPoisson extends TrucMarin{
  private ArrayList<Bulle> liBulle;
  private boolean etat;



  public GrosPoisson(int x, int y, float v){
    super(x,y,-v,0);
    this.liBulle= new ArrayList<Bulle> ();
    this.etat=true;

    }



    public void evolue(){
      Random generateurValeurs = new Random();
      int nb=generateurValeurs.nextInt(25)+1;
      if (nb ==5 ){this.etat = !this.etat;}
      if (this.etat){
        posX+=vitesseX;
        if(generateurValeurs.nextInt(10)+1==5){
          Bulle nouvBulle=new Bulle(posX,posY+3,generateurValeurs.nextInt(2)+1);
          liBulle.add(nouvBulle);
        }
      }
      else {
        posX-=vitesseX;
        if(generateurValeurs.nextInt(10)+1==5){
          Bulle nouvBulle=new Bulle(posX+26,posY+3,generateurValeurs.nextInt(2)+1);
          liBulle.add(nouvBulle);
        }
      }
      if (posX <-30 && this.etat){
        this.etat=false;
      }
      else if (posX > 180 && !this.etat){

        this.etat=true;}

      for (Bulle b:liBulle){
        b.evolue();

      }

    }





  public EnsembleChaines getEnsembleChaines(){
    EnsembleChaines ens= new EnsembleChaines();
    if (this.etat){

    ens.ajouteChaine(posX,posY+7,"              /^^^^^7","0xFF0000");
    ens.ajouteChaine(posX,posY+6,"         ,oO))))))))Oo,","0xFF0000");
    ens.ajouteChaine(posX,posY+5,"       ,'))))))))))))))), /{","0xFF0000");
    ens.ajouteChaine(posX,posY+4,"     ,'o  ))))))))))))))))={","0xFF0000");
    ens.ajouteChaine(posX,posY+3,"     >    ))))))))))))))))={","0xFF0000");
    ens.ajouteChaine(posX,posY+2,"     `,   ))))))\\\\ \\\\)))))))={","0xFF0000");
    ens.ajouteChaine(posX,posY+1,"       ',))))))))\\\\/)))))' \\\\{","0xFF0000");
    ens.ajouteChaine(posX,posY+0,"         '*O))))))))O*'","0xFF0000");
}
  else{

  ens.ajouteChaine(posX,posY+7,"              7^^^^^/","0xFF0000");
  ens.ajouteChaine(posX,posY+6,"         ,oO((((((((Oo,","0xFF0000");
  ens.ajouteChaine(posX,posY+5,"      }\\ ,(((((((((((((((',","0xFF0000");
  ens.ajouteChaine(posX,posY+4,"      }=((((((((((((((((  o',","0xFF0000");
  ens.ajouteChaine(posX,posY+3,"      }=((((((((((((((((    <","0xFF0000");
  ens.ajouteChaine(posX,posY+2,"    }=((((((// //(((((((   ,'","0xFF0000");
  ens.ajouteChaine(posX,posY+1,"    }// '((((((((//\\(((((,'","0xFF0000");
  ens.ajouteChaine(posX,posY+0,"           '*O((((((((O*'","0xFF0000");
  }
    for(int i=liBulle.size()-1;i>-1;i--){
      if(liBulle.get(i).getY()<50){
      ens.union(liBulle.get(i).getEnsembleChaines());
      }else{
        liBulle.remove(i);
      }
    }
    return ens;
  }
}
