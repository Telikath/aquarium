class PetitPoisson extends TrucMarin{
  private int type;
  public PetitPoisson(int x, int y, float v){
    super(x,y,v,0);
    this.type=(int)(Math.random()*2);
  }

  public EnsembleChaines getEnsembleChaines(){
    EnsembleChaines ens= new EnsembleChaines();
    if(type==0){
      ens.ajouteChaine(posX,posY,"><)))","0x000000");
      ens.ajouteChaine(posX+5,posY,"'>","0x0000FF");
    }else{
      ens.ajouteChaine(posX,posY,"><>","0x000000");
    }
    return ens;
  }
}
