class Algue{
  private int posX,posY,haut;
  private boolean etat;
  public Algue(int x, int y, int h){
    this.posX=x;
    this.posY=y;
    this.haut=h;
    this.etat=true;
  }
  public void evolue(){
    this.etat=!etat;
  }
  public EnsembleChaines getEnsembleChaines(){
    EnsembleChaines ens= new EnsembleChaines();
    if(etat){
      for(int i=0;i<this.haut;i++){
        if(i%2==0){
          ens.ajouteChaine(posX,posY+i+1," ))","0x32CD32");
        }else{
          ens.ajouteChaine(posX,posY+i+1,"(( ","0x32CD32");
        }
      }
    }else{
      for(int i=0;i<this.haut;i++){
        if(i%2==0){
          ens.ajouteChaine(posX,posY+i+1,"(( ","0x32CD32");
        }else{
          ens.ajouteChaine(posX,posY+i+1," ))","0x32CD32");
        }
      }
    }
    return ens;
  }

}
