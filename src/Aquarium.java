import java.util.ArrayList;
import java.util.Random;

class Aquarium{
	private int posX,posY;
	private int hauteur,largeur;
	private EnsembleChaines ens;
	private Chateau chat;
	private Sol sol;
	private Eau eau;
	private StatueDragon statue;
	ArrayList<PetitPoisson> liPetit;
	ArrayList<Algue> liAlgue;
	GrosPoisson grosP;
	public Aquarium(){
		Random generateurValeurs = new Random();
		this.hauteur=55;
		this.largeur=155;
		this.ens=new EnsembleChaines();
		this.liPetit=new ArrayList<PetitPoisson>();

		int x= generateurValeurs.nextInt(this.largeur)+1;
		int y = generateurValeurs.nextInt(45)+1;
		if(y<8){y=8;}
		float v =(float)(Math.random()*5)+1;
		this.grosP=new GrosPoisson(x,y,v);

		this.chat= new Chateau(5,1);
		this.sol= new Sol(0,0,this.largeur);
		this.eau= new Eau(0,50,this.largeur);
		this.statue= new StatueDragon(100,1);

		int nbPetit=generateurValeurs.nextInt(9)+1;
		for(int i=0;i<nbPetit;i++){
			x=generateurValeurs.nextInt(this.largeur)+1;
			y=generateurValeurs.nextInt(45)+1;;
			if(y>=50){y=49;}
			if(y<2){y=3;}
			v =(float)(Math.random()*5)+1;
			PetitPoisson petit=new PetitPoisson(x,y,v);
			this.liPetit.add(petit);
		}
		this.liAlgue=new ArrayList<Algue> ();
		int nbAlgue=generateurValeurs.nextInt(8)+1;
		for(int i=0;i<nbAlgue;i++){
			x=(int)(Math.random()*this.largeur);
			int h=generateurValeurs.nextInt(10)+5;
			Algue al=new Algue(x,0,h);
			this.liAlgue.add(al);
		}
	}
	public int getHauteur(){
		return this.hauteur;
	}
	public int getLargeur(){
		return this.largeur;
	}
	public EnsembleChaines getChaines(){
		return this.ens;
	}
	public void evolue(){
		this.ens.vider();

		for(PetitPoisson petit : liPetit){
			petit.evolue();
			if(petit.getX()>this.largeur){
				petit.setX(-5);
			}
			this.ens.union(petit.getEnsembleChaines());
		}

		this.grosP.evolue();

		this.ens.union(this.grosP.getEnsembleChaines());

		for(Algue algue : liAlgue){
			algue.evolue();
			this.ens.union(algue.getEnsembleChaines());
		}
		this.ens.union(this.chat.getEnsembleChaines());
		this.ens.union(this.sol.getEnsembleChaines());
		this.ens.union(this.eau.getEnsembleChaines());
		this.ens.union(this.statue.getEnsembleChaines());
	}
}
