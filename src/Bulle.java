class Bulle extends TrucMarin{
	public Bulle(int x,int y, float v){
		super(x,y,0,v);
	}
	public EnsembleChaines getEnsembleChaines(){
    EnsembleChaines ens= new EnsembleChaines();
    ens.ajouteChaine(posX,posY,"|_|","0x000000");
    ens.ajouteChaine(posX,posY+1," _ ","0x000000");
    return ens;
  }
}
