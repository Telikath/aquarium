class ChainePositionnee{
	  int posX,posY;
    String chaine;
    String couleur;
  	public ChainePositionnee(int posX,int posY, String chaine, String couleur)
		{
			this.chaine = chaine;
			this.posX = posX;
			this.posY = posY;
			this.couleur=couleur;
		}
		public int getX(){
			return this.posX;
		}

		public int getY(){
			return this.posY;
		}
		public String getChaine(){
			return this.chaine;
		}
}
