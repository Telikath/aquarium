class Chateau{
  private EnsembleChaines ens;
  public Chateau(int x,int y){
    this.ens= new EnsembleChaines();
    this.ens.ajouteChaine(x,y+7,"           I ","0xFF0000");
    this.ens.ajouteChaine(x,y+6,"         ,'.`. ","0xFF0000");
    this.ens.ajouteChaine(x,y+5,"   .__,-'.:::.`-.__, ","0xFF0000");
    this.ens.ajouteChaine(x,y+4,"    ~-------------~","0xFF0000");
    this.ens.ajouteChaine(x,y+3,"      _|=|___|=|_ ","0xFF0000");
    this.ens.ajouteChaine(x,y+2,".__,-'.:::::::::.`-.__,","0xFF0000");
    this.ens.ajouteChaine(x,y+1," ~-------------------~","0xFF0000");
    this.ens.ajouteChaine(x,y,  "    _|_|_|___|_|_|_","0xFF0000");
  }
  public EnsembleChaines getEnsembleChaines(){
    return ens;
  }
}
