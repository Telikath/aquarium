class StatueDragon{
  private EnsembleChaines ens;
  public StatueDragon(int x,int y){
    this.ens= new EnsembleChaines();
    this.ens.ajouteChaine(x,y+16,"           , , ","0x808080");
    this.ens.ajouteChaine(x,y+15,"          ddP' ","0x808080");
    this.ens.ajouteChaine(x,y+14,"        d8888b       /(","0x808080");
    this.ens.ajouteChaine(x,y+13,"       dY\"d888'     /M'  ","0x808080");
    this.ens.ajouteChaine(x,y+12,"     d88888888'   /dMb\\_,","0x808080");
    this.ens.ajouteChaine(x,y+11,"   d8888P`888P'  /dMMMP/","0x808080");
    this.ens.ajouteChaine(x,y+10,"   `V'    888' /dMMMMP/","0x808080");
    this.ens.ajouteChaine(x,y+9,"         d888P/dMMMMM( ","0x808080");
    this.ens.ajouteChaine(x,y+8,"        d88888dMMMMMMb\\","0x808080");
    this.ens.ajouteChaine(x,y+7,"       ,888PdPMMMMMMMMP'","0x808080");
    this.ens.ajouteChaine(x,y+6,"   dP88' 8888888b\\MMMMb\\","0x808080");
    this.ens.ajouteChaine(x,y+5,"  ``,d'   Y888888b\\MMMP' ","0x808080");
    this.ens.ajouteChaine(x,y+4,"    '      Y88888888\\M(              ,aaaaa,","0x808080");
    this.ens.ajouteChaine(x,y+3,"             Y8Pd8888\\M\\            V'    `aa,","0x808080");
    this.ens.ajouteChaine(x,y+2,"              d88888888\\\\, , ,            ,aaa","0x808080");
    this.ens.ajouteChaine(x,y+1,"           ,d8888Pd8888888V8V8bAvAvAvvAvd888P'","0x808080");
    this.ens.ajouteChaine(x,y,  "           8888P'  `Y8888888888888888888P\"\'","0x808080");
  }
  public EnsembleChaines getEnsembleChaines(){
    return ens;
  }
}
