class TrucMarin{
  protected int posX, posY;
  protected float vitesseX, vitesseY;
  TrucMarin(int x, int y, float vx, float vy){
    this.posX=x;
    this.posY=y;
    this.vitesseY=vy;
    this.vitesseX=vx;
  }
  public void evolue(){
    this.posY+=this.vitesseY;
    this.posX+=this.vitesseX;
  }
  public int getX(){
    return this.posX;
  }
  public int getY(){
    return this.posY;
  }
  public void setX(int x){
    this.posX=x;
  }
  public void setY(int y){
    this.posY=y;
  }
}
