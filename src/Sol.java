class Sol{
  private EnsembleChaines ens;
  public Sol(int x,int y, int largeur){
    this.ens= new EnsembleChaines();
    this.ens.ajouteChaine(x,y, this.generSol(largeur),"0xFFFF00");
  }

  private String generSol(int largeur){
    String res="";
    for (int i=0; i<largeur; i++){
      res=res+"@";
    }
    return res;
  }
  public EnsembleChaines getEnsembleChaines(){
    return ens;
  }
}
