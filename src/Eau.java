class Eau{
  private EnsembleChaines ens;
  public Eau(int x,int y, int largeur){
    this.ens= new EnsembleChaines();
    this.ens.ajouteChaine(x,y, this.generEau(largeur),"0x0000FF");
  }

  private String generEau(int largeur){
    String res="";
    for (int i=0; i<largeur; i++){
      res=res+"~";
    }
    return res;
  }
  public EnsembleChaines getEnsembleChaines(){
    return ens;
  }
}
